add_moderator:
    title: Moderator zu %forum% hinzufügen
    notice: Dieser Benutzer wurde zum Moderator ernannt.

ban_add:
    title: Eine IP Adresse sperren
    banned_notice: Die IP Adresse wurde gesperrt.

ban_form:
    ip: IP Adresse
    reason: Grund für Sperrung
    expiry_date: Läuft um (YYYY-MM-DD hh:mm) ab
    ban: Sperrung
    user: Benutzer mit der IP

ban_list:
    title: Sperrungen
    ip: IP Adresse
    timestamp: Zeit
    banned_by: Gesperrt von
    reason: Grund
    user: Benutzer
    user_none: kein
    expires: Expires
    expires_never: endet
    remove: Sperrungen entfernen

banned:
    title: Gesperrt
    message: Du wurdest gesperrt!

comments:
    author_deleted: '[gelöscht]'
    comments: Kommentare
    delete: Löschen
    delete_thread: Löschen mit Antworten
    edit: Bearbeiten
    permalink: Permalink
    parent: Parent
    reply: Antworten
    admin_ban: IP Sperrung
    form_load_error: Fehler beim Laden des Formulars. Bitte versuche es nochmal.
    info: '%user% schrieb %timestamp%'
    info_at_timestamp: um %timestamp%
    not_logged_in: Du musst %login_link% oder %register_link% um Kommentare zu schreiben.
    not_logged_in_login_link_label: Einloggen
    not_logged_in_register_link_label: Registrieren
    viewing_thread: Viewing a single comment thread
    thread_return: Alle Kommentare anzeigen
    return_to_forum: ← Zurück zu %forum%
    edit_info: (bearbeitet vor %edited_at%)
    moderator_info: (von einem Moderator vor %edited_at% bearbeitet)

comment_form:
    comment: Kommentar
    submit: Einreichen
    edit_title: Kommentar bearbeiten
    create_title: Kommentar einreichen

create_forum:
    create_new_forum: Neues Forum erstellen

edit_forum:
    edit_notice: Die Änderungen wurden gespeichert.
    delete_notice: Das Forum und alle Inhalte wurden gelöscht.
    title: '%forum% bearbeiten'

edit_user:
    title: Benutzer %username% bearbeiten

forum:
    moderators: Moderatoren
    manage: Manage
    edit: Forum bearbeiten
    subscribe: Abonnieren
    unsubscribe: Abbestellen
    subscriber_count: '{0} Keine Abonnenten|{1} %count% Abonnent|[1,Inf[ %count% Abonnenten'
    add_moderator: Moderator hinzufügen

forum_form:
    name: Name
    title: Titel
    description: Beschreibung
    create: Forum erstellen
    save: Änderungen speichern
    delete: Forum löschen
    confirm_delete: Are you sure you want to delete this forum and all its submissions and comments?
    confirm_delete: Bist du sicher, dass du dieses Forum und alle Einreichungen und Kommentare löschen willst?
    featured: Auf der Front Seite anzeigen

forum_list:
    name: Name
    title: Titel
    subscribers: Abonnenten
    page_title: Alle Foren auflisten
    create_forum: Forum erstellen
    submission_count: Einreichungen

forum_moderators:
    title: Moderatoren für /%forum_name%
    username: Benutzername
    since: Seit

front:
    subscribed_forums: Abonnierte Foren
    no_subscriptions: You are not subscribed to any forum. Showing featured forums instead.
    no_subscriptions: Du hast keine Foren abonniert. Es werden stattdessen die empohlenen Foren angezeigt.
    featured_forums: Empfohlene Foren
    no_forums: There are no featured forums to display.
    no_forums: Es können keine empfohlene Foren angezeigt werden.
    all: Alle
    featured: Empfohlen

inbox:
    title: Posteingang
    clear_inbox: Posteingang leeren
    clear_notice: Der Posteingang wurde geleert.
    empty: Der Posteingang ist leer.

login_form:
    log_in: Einloggen
    username: Benutzername
    password: Passwort
    reset_password: Passwort zurücksetzen?

markdown_type:
    help: Formatierungshilfe

moderator_form:
    user: Benutzername
    submit: Als Moderator hinzufügen

pagination:
    next: Nächste
    previous: Vorherige

request_password_reset:
    title: Request password reset link
    title: Anfrage zum Zurücksetzen des Passworts
    multiple_accounts_notice: >
        Wenn du mehrere Accounts mit einer Email Adresse registriert hast, wirst du jeweils ein Email pro Account erhalten
    no_confirmation_notice: >
        Wenn du keine Email erhaltest, ist deine angegebene Adresse vermutlich falsch. Aus Datenschutzgründen überprüfen wir nicht, ob Benutzer mit angegebener Email Adresse tatsächlich existieren.

request_password_reset_form:
    email: Email Adresse
    submit: Einreichen

reset_password:
    email_subject: '%site_name% - Passwort für Benutzer %username% zurücksetzen'

    # this should be kept at 76 columns --------------------------------------------
    email_body: |
        Wir haben die Anfrage erhalten, das Passwort für deinen %site_name%
        Account zurückzusetzen. Um dein Passwort zurückzusetzen, klicke
        auf den untenstehenden Link.

        %reset_link%

        Wenn die Anfrage nicht von dir stammt, kannst du diese Nachricht
        ignorieren. Der Link läuft nach 24 Stunden aus.

    email_notice: Eine Anfrage zum Zurücksetzen des Passworts wurde an die angegebene Email Adresse gesendet
    update_notice: Dein Passwort wurde aktualisiert.
    title: Passwort zurücksetzen

site_footer:
    version: Running %app% %version% (%branch%). Made with ☭.

site_nav:
    submit: Einreichen
    log_in: Einloggen
    log_out: Ausloggen
    profile: Profile
    register: Registrieren
    my_account: Mein Account
    user_settings: Einstellungen
    forum_list: Foren
    inbox: Posteingang (%count%)

submission_form:
    title: Titel
    url: URL
    body: Textkörper
    forum: Forum
    sticky: Sticky
    create: Inhalt einreichen
    edit: Einreichung bearbeiten
    delete: Einreichung löschen
    confirm_delete: Bist du dir sicher, dass du diese Einreichung löschen willst?

submissions:
    comments: '{0} Keine Kommentare|{1} %count% Kommentar|[1,Inf[ %count% Kommentare'
    delete_notice: Die Einreichung wurde gelöscht.
    edit: Bearbeiten
    edit_notice: Die Einreichung wurde bearbeitet.
    info_with_forum_name: Eingereicht von %submitter% %timestamp% in %forum%
    info_without_forum_name: Eingereicht von %submitter% %timestamp%
    info_at_timestamp: vor %timestamp%
    sort_by_hot: Beliebt
    sort_by_new: Neu
    sort_by_top: Top
    sort_by_controversial: Kontrovers
    total_votes: '{1} %count% Vote|[0,Inf[ %count% Votes'
    vote_stats: (+%up%, −%down%)
    ip_ban: IP Sperrung
    edit_info: (bearbeitet %edited_at%)
    moderator_info: (bearbeitet von einem Moderator %edited_at%)

time:
    earlier_format: 'vor %relative_time%'
    later_format: '%relative_time% später'

votes:
    upvote: Upvote
    downvote: Downvote
    retract_upvote: Upvote rückgängig machen
    retract_downvote: Downvote rückgängig machen

user:
    submissions: Einreichungen
    comments: Kommentare
    moderates: '%username% ist ein Moderator in:'
    username_rules: Erlaubte Zeichen sind A-Z, a-z, 0-9 und Underscore.
    password_rules: Mindestens 8, maximal 72 Zeichen auf Grund der Limitationen durch den bcrypt Algorithmus.
    email_optional: Providing an email address is optional. It will only be used for resetting your password.
    email_optional: Das Angeben einer Email Adresse ist freiwillig. Es wird ausschliesslich zum Zurücksetzen des Passworts verwendet.

user_form:
    username: Benutzername
    password: Passwort
    repeat_password: Passwort (wiederholen)
    new_password: Neues Passwort
    repeat_new_password: Neues Passwort (wiederholen)
    email: Email
    register: Registrieren
    save: Änderungen speichern

user_settings:
    title: Einstellungen von %username% bearbeiten
    update_notice: Einstellungen wurden aktualisiert.

user_settings_form:
    locale: Sprache
    night_mode: Nachtmodus
    save: Änderungen speichern
