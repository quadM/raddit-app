# Learn more about services, parameters and containers at
# http://symfony.com/doc/current/service_container.html
parameters:
    markdown_cache_time: 86400

    commonmark_config:
        html_input: escape

    html_purifier_config:
        AutoFormat.Linkify:           true   # Convert non-link URLs to links.
        Cache.DefinitionImpl:         null   # Disable cache
        HTML.Nofollow:                true   # Add rel="nofollow" to outgoing links.
        HTML.TargetBlank:             true   # Add target="_blank" to outgoing links.
        URI.DisableExternalResources: true   # Disable embedding of external resources like images.

services:
    _defaults:
        autoconfigure: true
        autowire: true
        public: false

    _instanceof:
        Doctrine\Common\EventSubscriber:
            tags: [doctrine.event_subscriber]

    # makes classes in src/AppBundle available to be used as services
    # this creates a service per class whose id is the fully-qualified class name
    Raddit\AppBundle\:
        resource: '../../*'
        # you can exclude directories or files
        # but if a service is unused, it's removed anyway
        exclude: '../../{Entity,Repository}'

    # controllers are imported separately to make sure they're public
    # and have a tag that allows actions to type-hint services
    Raddit\AppBundle\Controller\:
        resource: '../../Controller'
        public: true
        tags: ['controller.service_arguments']

    Raddit\AppBundle\EventListener\AttachIpToEntityListener:
        arguments: ["@request_stack"]
        tags:
            - { name: doctrine.event_listener, event: prePersist }

    Raddit\AppBundle\EventListener\LocaleListener:
        tags:
            - { name: kernel.event_listener, event: kernel.request, priority: 15 }
            - { name: kernel.event_listener, event: security.interactive_login, method: onInteractiveLogin }
            - { name: doctrine.event_listener, event: postPersist }
            - { name: doctrine.event_listener, event: postUpdate }

    Raddit\AppBundle\EventListener\SubmissionImageListener:
        calls:
            - [setLogger, ["@logger"]]
        arguments: ["@doctrine.orm.entity_manager", "@request_stack", "@validator"]
        tags:
            - { name: doctrine.event_listener, event: postPersist }
            - { name: kernel.event_listener, event: kernel.terminate }

    Raddit\AppBundle\Repository\BanRepository:
        factory: ["@doctrine.orm.entity_manager", "getRepository"]
        arguments: [Raddit\AppBundle\Entity\Ban]

    Raddit\AppBundle\Repository\ForumRepository:
        factory: ["@doctrine.orm.entity_manager", "getRepository"]
        arguments: [Raddit\AppBundle\Entity\Forum]

    Raddit\AppBundle\Repository\MessageThreadRepository:
        factory: ["@doctrine.orm.entity_manager", "getRepository"]
        arguments: [Raddit\AppBundle\Entity\MessageThread]

    Raddit\AppBundle\Repository\UserRepository:
        factory: ["@doctrine.orm.entity_manager", "getRepository"]
        arguments: [Raddit\AppBundle\Entity\User]

    Raddit\AppBundle\Twig\AppExtension:
        calls:
            - [setSiteName, ["%env(SITE_NAME)%"]]

    Raddit\AppBundle\Utils\CachedMarkdownConverter:
        calls:
            - [setExpiresAfter, ["%markdown_cache_time%"]]
        lazy: true


    ## Misc

    Doctrine\DBAL\Connection: "@doctrine.dbal.default_connection"

    Doctrine\Common\Persistence\ObjectManager: "@doctrine.orm.entity_manager"
    Doctrine\ORM\EntityManager: "@doctrine.orm.entity_manager"
    Doctrine\ORM\EntityManagerInterface: "@doctrine.orm.entity_manager"

    HTMLPurifier:
        class: HTMLPurifier
        arguments: ['@HTMLPurifier_Config']
        lazy: true

    HTMLPurifier_Config:
        class: HTMLPurifier_Config
        factory: [HTMLPurifier_Config, create]
        arguments:
            - "%html_purifier_config%"
        lazy: true

    League\CommonMark\CommonMarkConverter:
        arguments: ["%commonmark_config%", '@League\CommonMark\Environment']
        lazy: true

    League\CommonMark\Environment:
        factory: [League\CommonMark\Environment, createCommonMarkEnvironment]
        calls:
            - [addExtension, ['@Raddit\AppBundle\CommonMark\AppExtension']]
        lazy: true

    Symfony\Component\Serializer\Serializer: "@serializer"

    twig.extension.intl:
        class: Twig_Extensions_Extension_Intl
